/// @file
/// @brief PE_file Input/Output
#include "../include/PE_io.h"
#include <string.h>


///@brief Calculate offsets
///@param output_PE_file PE file data for calculate default offsets
/// @return true if calculate ok, else false
bool calculate_offset_PE_file(struct PEFile* output_PE_file){

    output_PE_file->magic_offset = PE_FILE_MAGIC_OFFSET + sizeof(output_PE_file->magic_offset);

    output_PE_file->header_offset = sizeof(output_PE_file->magic) + output_PE_file->magic_offset;

    output_PE_file->optional_header_offset = output_PE_file->header_offset + sizeof (struct PEHeader);

    output_PE_file->section_header_offset = output_PE_file->optional_header_offset + sizeof(struct OptionalHeader);

    return true;


}

///@brief Find section
///@param input_PE_file PE file data for search
///@param name_section name section
/// @return SectionHeader if search good, else empty SectionHeader
struct SectionHeader find_section_in_PE_file(struct PEFile* input_PE_file, const char* name_section){
    for (size_t i = 0; i < input_PE_file->header.number_of_sections; ++i) {
        if(strcmp(input_PE_file->section_headers[i].name, name_section) == 0){
            return input_PE_file->section_headers[i];
        }
    }
    return (struct SectionHeader ) {0};
}

///@brief Read PE file
///@param path_input_file char array of name file
///@param output_PE_file PE file for load data
/// @return true if read ok, else false
bool only_read_PE_file(const char* path_input_file, struct PEFile* output_PE_file){
    FILE* file_name = fopen(path_input_file, "rb");
    if (file_name != NULL){

        fseek(file_name,output_PE_file->magic_offset,SEEK_SET);
        if (fread(&output_PE_file->magic, sizeof(output_PE_file->magic), 1, file_name) != 1){
            return false;
        }

        fseek(file_name, output_PE_file->header_offset,SEEK_SET);
        if ( fread(&output_PE_file->header, sizeof(struct PEHeader), 1, file_name)!= 1){
            return false;
        }

        fseek(file_name,output_PE_file->optional_header_offset,SEEK_SET);
        if ( fread(&output_PE_file->optional_header, sizeof(struct OptionalHeader), 1, file_name)!= 1){
            return false;
        }

        fseek(file_name,output_PE_file->section_header_offset,SEEK_SET);

        output_PE_file->section_headers = malloc(sizeof(struct SectionHeader) * output_PE_file->header.number_of_sections);


        for (size_t i = 0; i < output_PE_file->header.number_of_sections; ++i) {
            if ( fread(&output_PE_file->section_headers[i], sizeof(struct SectionHeader),1 ,file_name)!= 1){
                return false;
            }
        }
        fclose(file_name);


        return true;

    } else{
        fclose(file_name);
        return false;
    }
}


///@brief Write PE file
///@param path_input_file char array of name file for reading
///@param path_output_file char array of name file for writing
///@param section Section Header to write
/// @return true if write ok, else false
bool only_write_PE_file(const char* path_input_file, const char* path_output_file, struct SectionHeader section){
    FILE* input_file = fopen(path_input_file, "rb");
    fseek(input_file, section.pointer_to_raw_data, SEEK_SET);
    char* data = malloc(section.size_of_raw_data);
    if ( fread(data, section.size_of_raw_data, 1, input_file)!= 1){
        return false;
    }

    FILE * output_file = fopen(path_output_file, "wb");
    fwrite(data, section.size_of_raw_data, 1, output_file);
    free(data);

    fclose(input_file);
    fclose(output_file);

    return true;
}
