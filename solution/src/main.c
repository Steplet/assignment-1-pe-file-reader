/// @file 
/// @brief Main application file

#include "../include/PE_io.h"

/// Application name string
#define APP_NAME "section-extractor"



/// @brief Entry point
/// @param argc Command lines number
/// @param argv Arguments

int main(int argc, char** argv)
{
    if (argc != 4){
        printf("Wrong numbers of args (T T)");
        return 0;
    }

    char* input_file_path = argv[1];
    char* section_name = argv[2];
    char* output_file_path = argv[3];

    struct PEFile* file = malloc(sizeof(struct PEFile));



    calculate_offset_PE_file(file);

    only_read_PE_file(input_file_path,file);


    only_write_PE_file(input_file_path,output_file_path, find_section_in_PE_file(file,section_name));

    free(file->section_headers);
    free(file);

    return 0;
}
