/// @file
/// @brief PE_file Header
#include <stdint.h>

/// Offset to
#define PE_FILE_MAGIC_OFFSET 0x3c

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
PEHeader{
    ///@name PE Header
    /// @{

    ///The number that identifies the type of target machine.
    uint16_t machine;

    /// The number of sections.
    uint16_t number_of_sections;

    /// Indicates when the file was created.
    uint32_t time_date_stamp;

    /// The file offset of the COFF symbol table
    uint32_t pointer_to_symbol_table;

    /// The number of entries in the symbol table
    uint32_t number_of_symbols;

    /// The size of the optional header
    uint16_t size_of_optional_header;

    /// The flags that indicate the attributes of the file
    uint16_t characteristics;
    ///@}

};



struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
OptionalHeader{
    ///@name Optional Header
    ///@{

    /// The unsigned integer that identifies the state of the image file.
    uint16_t magic;

    /// The linker major version number.
    uint8_t major_linker_version;

    /// The linker minor version number.
    uint8_t minor_linker_version;

    /// The size of the code (text) section
    uint32_t size_of_code;

    /// The size of the initialized data section
    uint32_t SizeOfInitializedData;

    /// The size of the uninitialized data section
    uint32_t SizeOfUninitializedData;

    /// The address of the entry point relative to the image base
    uint32_t AddressOfEntryPoint;

    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
    uint32_t BaseOfCode;

    /// @}
};


struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
SectionHeader{
    /// @name SectionHeader
    ///@{

    /// An 8-byte, null-padded UTF-8 encoded string
    char name[8];

    /// The total size of the section when loaded into memory.
    uint32_t virtual_size;

    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
    uint32_t virtual_address;

    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t size_of_raw_data;

    /// The file pointer to the first page of the section within the COFF file.
    uint32_t pointer_to_raw_data;

    /// The file pointer to the beginning of relocation entries for the section.
    uint32_t pointer_to_relocations;

    /// The file pointer to the beginning of line-number entries for the section.
    uint32_t pointer_to_linenumbers;

    /// The number of relocation entries for the section.
    uint16_t number_of_relocations;

    /// The number of line-number entries for the section.
    uint16_t number_of_linenumbers;

    /// The flags that describe the characteristics of the section.
    uint32_t characteristics;
    /// @}

};



struct
#if defined __clang__ || defined __GNUC__
        __attribute__((packed))
#endif
PEFile
{
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Optional header
    struct OptionalHeader optional_header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;
    ///@}
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif


