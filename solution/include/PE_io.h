/// @file
/// @brief PE_file Input/Output Header

#include "PE_file.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>



///@brief Read PE file
///@param path_input_file char array of name file
///@param output_PE_file PE file for load data
/// @return true if read ok, else false
bool only_read_PE_file(const char* path_input_file, struct PEFile* output_PE_file);

///@brief Write PE file
///@param path_input_file char array of name file for reading
///@param path_output_file char array of name file for writing
///@param section Section Header to write
/// @return true if write ok, else false
bool only_write_PE_file(const char* path_input_file, const char* path_output_file, struct SectionHeader section);

///@brief Calculate offsets
///@param output_PE_file PE file data for calculate default offsets
/// @return true if calculate ok, else false
bool calculate_offset_PE_file(struct PEFile* output_PE_file);

///@brief Find section
///@param input_PE_file PE file data for search
///@param name_section name section
/// @return SectionHeader if search good, else empty SectionHeader
struct SectionHeader find_section_in_PE_file(struct PEFile* input_PE_file, const char* name_section);


